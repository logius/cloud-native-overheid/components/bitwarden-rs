# Bitwarden_rs
A vault for installation in Kubernetes. With browser UI, possibility to give multiple users access to the same secrets,
to store secrets in organizations each with their own authorization, etc.

Distinguishes itself from Bitwarden because this implementation in Rust can be deployed on premise.

Refer to https://github.com/dani-garcia/bitwarden_rs for this implementation of Bitwarden and https://github.com/gissilabs/charts for the chart used.
